import uuid
from datetime import datetime

from finance import Bill
from saloon import Table


class Order:
	def __init__(self, item_dict, in_out, table_number, **kwargs):
		self.uuid = uuid.uuid4()
		self.item_dict = item_dict
		self.in_out = in_out
		self.datetime = datetime.datetime.now()
		self.bill = self.create_bill()
		self.table = self.assign_table(table_number)
		super().__init__(**kwargs)
	
	@staticmethod
	def assign_table():
		return Table.table_list()
	
	def create_bill(self):
		return Bill(self.total_price)
	
	@property
	def total_price(self):
		return 1000
	
	@classmethod
	def sample(cls):
		return cls(
			uuid=15463,
			item_dict={
				"food_list": "pizza", "beverage": "cola",
				"starter": "salad"
			}
			, in_out="in",
			datetime=datetime.now(), bill=Bill.sample(), table="5"
		)
