import uuid
from datetime import datetime

FOOD_TYPE = "FOOD"
BEVERAGE_TYPE = "BEVERAGE"


class Item:
	food_list = list()
	beverage_list = list()
	starter_list = list()
	item = list()
	
	def __init__(self, name, item_type, price, **kwargs):
		self.uuid = uuid.uuid4()
		self.name = name
		self.item_type = item_type
		self.price = price
		self.datetime = datetime.datetime.now()
		super().__init__(**kwargs)
		
		if item_type.lower() == FOOD_TYPE.lower():
			Item.food_list.append(self)
		
		elif item_type.lower() == BEVERAGE_TYPE.lower():
			Item.beverage_list.append(self)
		else:
			Item.starter_list.append(self)
	
	@classmethod
	def search(cls, uuid=None, item_id=None):
		id_temp = "uuid"
		temp = uuid
		if uuid is None:
			id_temp = "item_id"
			temp = item_id
		temp_list = list()
		temp_list.extend(cls.food_list)
		temp_list.extend(cls.starter_list)
		temp_list.extend(cls.beverage_list)
		for item in temp_list:
			if temp in getattr(item, id_temp):
				return item
		return None
	
	@classmethod
	def sample(cls):
		return cls(
			uuid=12562, name="pizza", item_type="food", price=1500,
			datetime=datetime.now()
		)


erfan = Item.sample()
