class Supervisor:
	def __init__(self, username, password, phone_number, **kwargs):
		self.username = username
		self.password = password
		self.phone_number = phone_number
		super().__init__(**kwargs)
	
	@classmethod
	def sample(cls):
		return cls(username="erfan", password=123, phone_number=+989123222355)
