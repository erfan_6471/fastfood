import datetime
import uuid


class Bill:
	def __init__(self, total_price, **kwargs):
		self.uuid = uuid.uuid4()
		self.total_price = total_price
		self.payment = self.create_payment()
		super().__init__(**kwargs)
	
	@staticmethod
	def create_payment():
		return 5000
	
	@classmethod
	def sample(cls):
		return cls(uuid=125463, total_price=1500, payment=True)


class Payment:
	def __init__(self, payment_type, is_paid, price, **kwargs):
		self.uuid = uuid.uuid4()
		self.payment_type = payment_type
		self.is_paid = is_paid
		self.datetime = datetime.datetime.now()
		self.price = price
		super().__init__(**kwargs)
	
	@classmethod
	def sample(cls):
		return cls(
			uuid=25652, payment_type="online", is_paid=True,
			datetime=datetime.datetime.now(), price=1500
		)
