import uuid


class Table:
	table_list = list()
	
	def __init__(self, capacity, number, reserved, is_available, **kwargs):
		self.uuid = uuid.uuid4()
		self.capacity = capacity
		self.number = number
		self.reserved = reserved
		self.is_available = is_available
		super().__init__(**kwargs)
	
	@classmethod
	def sample(cls):
		return cls(
			uuid=125652, capacity=25, number=25, reserved=None,
			is_available=True
		)
